//
//  CustoAlert.swift
//  Picnic
//
//  Created by InnovacionVO on 06/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import Foundation
import UIKit


class CustoAlert{

    struct Constants {
        static let backgroundAlphaTo:CGFloat = 0.6
        
    }
   
    private let backgroundView:UIView = {
        let background = UIView()
        background.backgroundColor = .black
        background.alpha = 0.6
        return background
    }()
    
    private let alertView:UIView = {
        let alert = UIView()
        alert.backgroundColor = .white
        alert.layer.masksToBounds = true
        return alert
    }()
    
    private var textView:UITextField?
    private var btn:UIButton?
    
    func showAlert(with hint:String, type:String, on ViewController:UIViewController){
        guard let targetView = ViewController.view else {
            return
        }
        backgroundView.frame = targetView.bounds
        alertView.alpha = 1
        switch type {
        case "edittext":
            alertView.frame = CGRect(x: 0 , y: (targetView.frame.size.height/2), width: targetView.frame.size.width, height: 55)
            
            let icon = UIImageView(frame: CGRect(x: 0, y: 7.5 , width: 40, height: 40))
            icon.image = #imageLiteral(resourceName: "ic_comment")
            icon.contentMode = .center
            
            textView = UITextField(frame: CGRect(x: 48, y: 8, width: targetView.frame.size.width - 93, height: 40))
            textView!.placeholder = hint
            textView!.returnKeyType = .done
            textView?.becomeFirstResponder()
            
            
            btn = UIButton(frame: CGRect(x: textView!.frame.width + 43 , y: 8, width: 40, height: 40))
            btn!.cornerRadius = 5
            switch ViewController {
            case is ProductDetailsViewController:
                btn!.addTarget(ViewController.self, action: #selector((ViewController as! ProductDetailsViewController).actionAlert ), for: .touchUpInside)
            default: break
                
            }
            
            btn!.backgroundColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
            btn!.setTitle("OK", for: .normal)
            btn!.setTitleColor(.white, for: .normal)
            
            
            
            alertView.addSubview(btn!)
            alertView.addSubview(icon)
            alertView.addSubview(textView!)
            
        default:
            alertView.frame = CGRect(x: (targetView.frame.size.width/2) - ((targetView.frame.size.width - 80)/2), y: (targetView.frame.size.height/2) - 175, width: (targetView.frame.size.width - 80), height: 350)
            alertView.cornerRadius = 12
            
        }
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        
        UIView.animate(withDuration: 0.25) {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        }
    }

    
    func dismissAlert(){
        if(textView != nil){
            textView?.resignFirstResponder()
        }
        
        alertView.alpha = 0
        
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
            }
        })
    }
    
    func dismissAlertWhitResponse()-> String{
        
        var response = ""
        
        if(textView != nil){
            textView?.resignFirstResponder()
            response = (textView?.text!)!
            textView?.text = ""
        }
        
        alertView.alpha = 0
        
        UIView.animate(withDuration: 0.25,animations:  {
            self.backgroundView.alpha = 0
        }, completion: { done in
            if(done){
                self.alertView.removeFromSuperview()
                self.textView?.removeFromSuperview()
                self.backgroundView.removeFromSuperview()
                self.btn?.removeFromSuperview()
            }
        })
        
        return response
    }
}
