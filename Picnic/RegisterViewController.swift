//
//  RegisterViewController.swift
//  Picnic
//
//  Created by MacBook Pro on 22/10/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtNameRegister: UITextField!
    @IBOutlet weak var txtPhoneRegister: UITextField!
    @IBOutlet weak var txtMailRegister: UITextField!
    @IBOutlet weak var txtAddressRegister: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNameRegister.attributedPlaceholder = NSAttributedString(string: "Nombre", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtPhoneRegister.attributedPlaceholder = NSAttributedString(string: "Telefono", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtMailRegister.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        txtAddressRegister.attributedPlaceholder = NSAttributedString(string: "Direccion", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
