//
//  ViewController.swift
//  Picnic
//
//  Created by MacBook Pro on 19/10/17.
//  Copyright © 2017 MonkeyValley. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var viewBackImage: UIView!
    @IBOutlet weak var imgeFront: UIImageView!
    
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnEnter: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        viewBackImage.layer.cornerRadius = viewBackImage.frame.size.height/2
        viewBackImage.clipsToBounds = true
        
        imgeFront.layer.shadowColor = UIColor.white.cgColor
        imgeFront.layer.shadowRadius = 50
        imgeFront.layer.shadowOpacity = 3.0
        imgeFront.layer.shadowOffset = CGSize(width: 10, height: 0)
        
        
        
        imgeFront.frame.size.width = view.frame.size.width
        imgeFront.frame.size.height = view.frame.size.height
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

