//
//  ProductDetailsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 06/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class ProductDetailsViewController: UIViewController,UITextFieldDelegate {
   
    
    
    @IBOutlet weak var btnAddNotes:UIButton!
    let customAlert = CustoAlert()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func ActionAddNotes(_ sender:UIButton){
        customAlert.showAlert(with: "Agregar notas", type: "edittext", on: self)
    }
    
    @objc func actionAlert(){
        let response = customAlert.dismissAlertWhitResponse()
        print("hello \(response)")
        
        
    }

}
