//
//  PolicyWVViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 11/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import WebKit
import HorizontalProgressBar

class PolicyWVViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var progressBar: UIView!
    
    private var progressBarV:HorizontalProgressbar?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.navigationDelegate = self
        let url = URL(string: config.url_policy_privacy)!
        webView.load(URLRequest(url: url))
        
        progressBarV = HorizontalProgressbar(frame: CGRect(x: 0, y: (progressBar.frame.size.height) - 3, width: (progressBar.frame.size.width), height: 3))
        progressBarV!.noOfChunks = 2
        progressBarV!.kChunkWdith = 150
        progressBarV!.progressTintColor = UIColor.red
        progressBar.addSubview(progressBarV!)

    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        progressBarV!.startAnimating()
        progressBar.isHidden = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressBarV!.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        progressBarV!.stopAnimating()
        progressBar.isHidden = true

    }
    
}
