//
//  SendMessageViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 11/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class SendMessageViewController: UIViewController {


    @IBOutlet weak var checkbox_food: UIButton!
    @IBOutlet weak var checkbox_service: UIButton!
    @IBOutlet weak var checkbox_other: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionCheckFood(_ sender: UIButton) {
        
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "ic_check"), for: .normal)
            sender.tag = 1
        }else{
            sender.setImage(UIImage(named: "ic_unchecked"), for: .normal)
            sender.tag = 0
        }
        
    }
    
    @IBAction func actionCheckService(_ sender: UIButton) {
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "ic_check"), for: .normal)
            sender.tag = 1
        }else{
            sender.setImage(UIImage(named: "ic_unchecked"), for: .normal)
            sender.tag = 0
        }
    }
    
    @IBAction func actionCheckOther(_ sender: UIButton) {
        if(sender.tag == 0){
            sender.setImage(UIImage(named: "ic_check"), for: .normal)
            sender.tag = 1
        }else{
            sender.setImage(UIImage(named: "ic_unchecked"), for: .normal)
            sender.tag = 0
        }
    }
}
