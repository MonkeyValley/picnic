//
//  PaymentMethodViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 05/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class PaymentMethodViewController: UIViewController {

    @IBOutlet weak var tcMethods: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionNewCrad(_ sender:UIButton){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "addmethodpayentcontroller") as! AddMethodPayViewController
        self.present(vc, animated: true, completion: nil);
    }
}

extension PaymentMethodViewController:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "methodsCell", for: indexPath) as! MethosPayTableViewCell
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 63.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "addmethodpayentcontroller") as! AddMethodPayViewController
        self.present(vc, animated: true, completion: nil);
        
    }
}
