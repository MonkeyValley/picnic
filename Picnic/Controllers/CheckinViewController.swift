//
//  CheckinViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 11/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit
import Lottie

class CheckinViewController: UIViewController {

    @IBOutlet weak var animView: AnimationView!
    private var animatView: AnimationView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//
        animatView = .init(name: "anim_qrcode")
        animatView!.frame = animView.bounds
        animatView!.frame.size.width = view.frame.width - 40
        animatView!.loopMode = .loop
        animatView!.contentMode = .scaleAspectFill
        animView.addSubview(animatView!)
        animatView!.play()
    }
    
    @IBAction func actionContinue(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
}
