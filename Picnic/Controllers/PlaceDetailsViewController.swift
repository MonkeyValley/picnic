//
//  PlaceDetailsViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 21/09/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class PlaceDetailsViewController: UIViewController, UIScrollViewDelegate{
    
    @IBOutlet weak var tvDetailsPlace: UITableView!
    @IBOutlet weak var svenuSlider: UIScrollView!
    @IBOutlet weak var lblDescriptionPlaceDetails: UILabel!
    @IBOutlet weak var imgPlaceDetails: UIImageView!
    @IBOutlet weak var lblTitlePlaceDetails: UILabel!
    
    var arrayCategories = [CategoriesPlace]()
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var pageNumber:CGFloat = 0.0
    var categorieSelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getData()
        initenu()
    }
    
    func getData(){
        
        arrayCategories.append(CategoriesPlace(_id: "5e28b77e7a012b4e1db58bb2", status: true, bActivo: true, cDescripcion: "Sushi", nIdRestaurante: "59ebd6b75461429d2e9c7d3a", lstPlatillos: [Dishes(lstIngredientes: [""], lstPreparacion: [""], lstTamanios: [""], _id: "5e28ba9b55841b7090d6d2e7", categoria: "5e28b77e7a012b4e1db58bb2", bDisponible: true, bActivo: true, cFoto: "1jAF0OVFEMHpVPKLCZJ7cHxI0OqoZPEFU", cDescripcion: "Rollo de pollo con pulpo", cNombrePlatillo: "Autentico", nIdCategoria: "5e28b77e7a012b4e1db58bb2"), Dishes(lstIngredientes: [""], lstPreparacion: [""], lstTamanios: [""], _id: "5e28ba9b55841b7090d6d2e7", categoria: "5e28b77e7a012b4e1db58bb2", bDisponible: true, bActivo: true, cFoto: "1jAF0OVFEMHpVPKLCZJ7cHxI0OqoZPEFU", cDescripcion: "rollo natural de Cameron", cNombrePlatillo: "Guerra", nIdCategoria: "5e28b77e7a012b4e1db58bb2") ]) )
        
         arrayCategories.append(CategoriesPlace(_id: "5e28b9b355841b7090d6d2e1", status: false, bActivo: true, cDescripcion: "Bocadillos", nIdRestaurante: "59ebd6b75461429d2e9c7d3a", lstPlatillos: [Dishes(lstIngredientes: [""], lstPreparacion: [""], lstTamanios: [""], _id: "5e608971477f3c0b542752ca", categoria: "5e28b9b355841b7090d6d2e1", bDisponible: true, bActivo: true, cFoto: "", cDescripcion: "Chiles rellenos de Philadelphia", cNombrePlatillo: "Chiles rellenos", nIdCategoria: "5e28b9b355841b7090d6d2e1") ]) )
        
         arrayCategories.append(CategoriesPlace(_id: "5e40b160e59e2f248dbebaf5", status: false, bActivo: true, cDescripcion: "Guisos", nIdRestaurante: "59ebd6b75461429d2e9c7d3a", lstPlatillos: [Dishes(lstIngredientes: [""], lstPreparacion: [""], lstTamanios: [""], _id: "5e40b1cfe59e2f248dbebaf6", categoria: "5e40b160e59e2f248dbebaf5", bDisponible: true, bActivo: true, cFoto: "16O8-sj68yPlls6xIpUewqOnJXhniNavk", cDescripcion: "Guiso de verduras con res, pollo, camarón o mixto", cNombrePlatillo: "Teriyaqui", nIdCategoria: "5e40b160e59e2f248dbebaf5") ]) )
        
        
    }
    
    func initenu(){
        
        let widthitem = self.view.frame.size.width / 3.5
        let subViews = self.svenuSlider.subviews
        for subview in subViews{
            subview.removeFromSuperview()
        }
        
        svenuSlider.contentSize = CGSize(width: widthitem * CGFloat(arrayCategories.count), height:self.svenuSlider.frame.size.height)
        svenuSlider.delegate = self
        
                for itme in 0..<arrayCategories.count {
                    
                    frame.origin.x = (widthitem) * CGFloat(itme)
                    frame.size = CGSize(width: widthitem , height: self.svenuSlider.frame.size.height)
                    
                    let conteiner = UIView(frame: frame)
                    let gesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
                    conteiner.addGestureRecognizer(gesture)
                    conteiner.tag = itme
                    let label = UILabel()
                    label.textColor = UIColor.darkGray
                    label.text = arrayCategories[itme].cDescripcion
                    label.frame.size = CGSize(width: conteiner.frame.width, height: conteiner.frame.height)
                    label.frame.origin.x = 0
                    label.frame.origin.y = 0
                    label.font = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
                    label.textAlignment = .center
                    
                    if(arrayCategories[itme].status){
                        let indicator = UIView(frame: CGRect(x: 0, y: conteiner.frame.height - 2, width: conteiner.frame.width, height: 2))
                        indicator.backgroundColor = UIColor(red: 254/255, green: 88/255, blue: 34/255, alpha: 1.0)
                        conteiner.addSubview(indicator)
                    }
                    
                    conteiner.addSubview(label)
                    self.svenuSlider.addSubview(conteiner)
                    
                }
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        let tag = view.tag

        arrayCategories.forEach { (it) in
            it.status = false
        }
        //self.svenuSlider.contentOffset.x = view.frame.origin.x
        self.categorieSelected = tag
        self.arrayCategories[tag].status = true
        self.initenu()
        self.tvDetailsPlace.reloadData()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageNumber = svenuSlider.contentOffset.x / (self.view.frame.size.width/3.5)
        print(pageNumber)
        
        
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        pageNumber = svenuSlider.contentOffset.x / (self.view.frame.size.width/3.5)
        print(pageNumber)
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PlaceDetailsViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCategories[self.categorieSelected].lstPlatillos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! DetailsPlaceTableViewCell
        
        cell.lblTitleDish.text = arrayCategories[self.categorieSelected].lstPlatillos[indexPath.row].cNombrePlatillo
        cell.lblDescriptionDish.text = arrayCategories[self.categorieSelected].lstPlatillos[indexPath.row].cDescripcion
        cell.lblPriceDish.text = "$100"
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "productdetailscontroller") as! ProductDetailsViewController
        self.present(vc, animated: true, completion: nil);
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    
}

