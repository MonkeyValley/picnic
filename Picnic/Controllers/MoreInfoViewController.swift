//
//  MoreInfoViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 10/10/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class MoreInfoViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func actionSendMessagge(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "sendMessageController") as! SendMessageViewController
        self.present(vc, animated: true, completion: nil);
        
    }
    @IBAction func actionSeePolicy(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "policyWVController") as! PolicyWVViewController
        self.present(vc, animated: true, completion: nil);
    }
}
