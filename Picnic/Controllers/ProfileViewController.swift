//
//  ProfileViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 21/09/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    
    
    @IBOutlet weak var tvProfileMenu: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
extension ProfileViewController:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellProfileMenu", for: indexPath) as! ProfileTableViewCell
        
        switch indexPath.row {
        case 0:
            cell.lblTitleItemMenu.text = "Formas de pago"
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "creditcard", withConfiguration: homeSymbolConfiguration)
            break
        case 1:
            cell.lblTitleItemMenu.text = "Mis direcciones"
            
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "location.fill", withConfiguration: homeSymbolConfiguration)
            break
        case 2:
            cell.lblTitleItemMenu.text = "Descuentos"
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "flame.fill", withConfiguration: homeSymbolConfiguration)
            break
        case 3:
            cell.lblTitleItemMenu.text = "Check-in"
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "qrcode.viewfinder", withConfiguration: homeSymbolConfiguration)
            break
        case 4:
            cell.lblTitleItemMenu.text = "Sugerir restaurante"
            cell.iconItemMenu.image = UIImage(named: "ic_suggest_menu")
            break
        case 5:
            cell.lblTitleItemMenu.text = "Más información"
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "questionmark.diamond", withConfiguration: homeSymbolConfiguration)
            break
        case 6:
            cell.lblTitleItemMenu.text = "Cerrar sesión"
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "escape", withConfiguration: homeSymbolConfiguration)
            break
        default:
            cell.lblTitleItemMenu.text = ""
            let homeSymbolConfiguration = UIImage.SymbolConfiguration(pointSize: 24, weight: .regular)
            cell.iconItemMenu.image = UIImage(systemName: "close", withConfiguration: homeSymbolConfiguration)
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            
                let storyboard = UIStoryboard(name: "Main", bundle: nil);
                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentMethosController") as! PaymentMethodViewController
                self.present(vc, animated: true, completion: nil);
               
            
            break
        case 1:
            
            break
        case 2:
            break
        case 3:
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let vc = storyboard.instantiateViewController(withIdentifier: "checkinController") as! CheckinViewController
            self.present(vc, animated: true, completion: nil);
            break
        case 4:
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let vc = storyboard.instantiateViewController(withIdentifier: "suggestController") as! SuggestRestaurantViewController
            self.present(vc, animated: true, completion: nil);
            break
        case 5:
            let storyboard = UIStoryboard(name: "Main", bundle: nil);
            let vc = storyboard.instantiateViewController(withIdentifier: "moreInfoController") as! MoreInfoViewController
            self.present(vc, animated: true, completion: nil);
            
            break
        default:
            break
        }
        
    }
    
    
}
