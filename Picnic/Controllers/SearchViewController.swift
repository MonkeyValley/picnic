//
//  SearchViewController.swift
//  Picnic
//
//  Created by InnovacionVO on 11/09/20.
//  Copyright © 2020 Monkey Valley. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController{

    @IBOutlet weak var cvSearchGrill: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func btnSeeAllRestaurants(_ sender: UIButton) {
        showRestList()
    }
    
    func showRestList(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil);
        let vc = storyboard.instantiateViewController(withIdentifier: "AllBranchesController") as! AllPlacesViewController
        self.present(vc, animated: true, completion: nil);
    }
}
extension SearchViewController:UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
           return 5
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = collectionView.dequeueReusableCell(withReuseIdentifier: "searchItem", for: indexPath) as! SerachGrillCollectionViewCell
        
        
        let imgBack = UIImageView(frame: item.bounds)
        imgBack.image = UIImage(named: "cat_china")
        imgBack.backgroundColor = UIColor.red
        imgBack.contentMode = .scaleAspectFill
        item.addSubview(imgBack)
        
        
        let descriptionView = UIView(frame: CGRect(x: 16, y: 130, width: 110, height: 60) )
        descriptionView.backgroundColor = .white
        descriptionView.cornerRadius = CGFloat(10.0)
        item.addSubview(descriptionView)
        
        let lblDescriptionFood = UILabel(frame: CGRect(x: 0, y: 0, width: 110, height: 40))
        lblDescriptionFood.textAlignment = .center
        lblDescriptionFood.font = UIFont(name: "Helvetica", size: 30.0)
        lblDescriptionFood.text = "🥡🍙"
        descriptionView.addSubview(lblDescriptionFood)
        
        let lblNumberFood = UILabel(frame: CGRect(x: 0, y: 40, width: 90, height: 20))
        lblNumberFood.font = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        lblNumberFood.textAlignment = .right
        lblNumberFood.text = "(x3)"
        descriptionView.addSubview(lblNumberFood)
        
        return item
       }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "HeaderSerachCollectionReusableView", for: indexPath) as! HeaderSerachCollectionReusableView
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.handlerPromotions(_:)))
        header.addGestureRecognizer(gesture)
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showRestList()
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (view.frame.width / 2.2) - 10, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 280)
    }
        
    @objc func handlerPromotions(_ sender:UITapGestureRecognizer){
        print("HANDLER")
        self.showRestList()
    }
    

}
